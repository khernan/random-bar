package com.example.randombar;

import android.content.Intent;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.seismic.ShakeDetector;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ResultadoActivity extends AppCompatActivity implements ShakeDetector.Listener {
    //Toast.makeText(ResultadoActivity.this, "Texto aca", Toast.LENGTH_LONG).show();
    private static final String TAG = MainActivity.class.getSimpleName();
    private final Bar randomBar = new Bar();
    private ShakeDetector shakeDetector = new ShakeDetector(this);
    SensorManager sensorManager;
    private MediaPlayer mp_prindis;
    Animation anim_rotate;
    private MediaPlayer mp_cat;
    private boolean existLastResult = false;
    private boolean existError = false;
    private String errorMsg;
    Bundle extras;
    static public int countAux = 5; //Eliminar al terminar, usado para debbugear


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        extras = getIntent().getExtras();
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mp_prindis = MediaPlayer.create(this, R.raw.brindis);
        anim_rotate = AnimationUtils.loadAnimation(ResultadoActivity.this, R.anim.rotate_custom);
        mp_cat = MediaPlayer.create(this, R.raw.cat);
        setBotonQuieroIrYa();
        setShakeSuggest();
        //Entramos cuando doblamos la pantalla desde la primera vez en adelante
        if(savedInstanceState != null){
            //Si sale un error debo inicializar ambos en null, y solo si el estado lo cambia cambiara
            boolean existLastResultAux = savedInstanceState.getBoolean("existLastResult");
            boolean existErrorAux = savedInstanceState.getBoolean("existError");
            String errorMsg;
            //Si ya habiamos encontrado algo, lo llamamos nuevamente
            if(existLastResultAux){
                setResultValues(BarHelper.getLastResult(),false);
            }
            if(existErrorAux){
                errorMsg = savedInstanceState.getString("errorMsg");
                setNoResultsFoundLayout(errorMsg, false);
            }
        }
        //Si no la
        else{
            setSearchResult();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        shakeDetector.start(sensorManager);
    }

    @Override
    protected void onPause() {
        super.onPause();
        shakeDetector.stop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(existLastResult){
            outState.putBoolean("existLastResult", existLastResult);
        }
        if(existError){
            outState.putBoolean("existError", existError);
            outState.putString("errorMsg",errorMsg);
        }
    }

    @Override
    public void hearShake() {
        Bar bar = BarHelper.getRandomBar(true);
        if(bar != null){
            setResultValues(bar,true);
        }

    }

    private void setSearchResult(){
        double myLat = extras.getDouble("myLat");
        double myLng = extras.getDouble("myLng");
        int radiusInMeters = extras.getInt("maxDistance");
        final double minRating = extras.getDouble("minRating");
        BarHelper.vaciarResultados();

        String keyword = OpcionesActivity.getKeyword();

        String key= getString(R.string.places_api_key);
        String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+myLat+","+myLng+"&radius="+radiusInMeters+"&keyword="+keyword+"&opennow&key="+key;
        //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-34.6113882,-58.4297874&radius=500&keyword=bar&opennow&key=AIzaSyDxT4q4mX3o_re7XCfFXJHlwAgLK-05FzU
        //Hacemos el HTTP Request
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) { }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String myResponse = response.body().string();
                    try {
                        //Guardamos la lista de resultados
                        JSONObject jsonObject = new JSONObject(myResponse);
                        JSONArray results = (JSONArray) jsonObject.get("results");

                        BarHelper.actualizarResultadosJson(results);
                        BarHelper.filtrarResultados(minRating);

                        if(results.length()>0){
                            Bar bar = BarHelper.getRandomBar(true);
                            if(bar != null){
                                setResultValues(bar,true);
                            }
                            else {
                                //Los bares encontrados no cumplen el rating minimo
                                setNoResultsFoundLayout(getString(R.string.resultado_activity_notfound_rating),true);
                            }
                        }
                        else{
                            //No se encontro ningun bar en el rango
                            setNoResultsFoundLayout(getString(R.string.resultado_activity_notfound_radius), true);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private  void setNoResultsFoundLayout(final String state, final boolean sound){
        existError = true;
        errorMsg = state;
        if(sound){
            mp_cat.start();
        }
        ResultadoActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setContentView(R.layout.no_results_layout);
            }
        });

        ResultadoActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView tv_no_results_msg = findViewById(R.id.tv_no_results_msg);
                tv_no_results_msg.setText(state);
            }
        });
    }

    private void setResultValues(final Bar bar, final boolean sound){
        existLastResult = true;
        if(sound){
            mp_prindis.start();
        }
        ResultadoActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView name = findViewById(R.id.tv_name);
                TextView direction = findViewById(R.id.tv_direccion);
                TextView rating = findViewById(R.id.tv_rating);
                RatingBar rb_result_rating = (RatingBar) findViewById(R.id.rb_result_rating);
                TextView totalResults = findViewById(R.id.tv_totalresults);
                ProgressBar progressBar = findViewById(R.id.progressBar2);
                ImageView ivBotonFlecha = findViewById(R.id.iv_quiero_ir_ya);
                TextView tv_shakeIt = findViewById(R.id.tv_shakeIt);
                TextView tv_result_state = findViewById(R.id.tv_result_state);
                //Setea los valores que vemos en la pantalla
                name.setText(bar.getName());
                direction.setText(bar.getVicinity());
                rating.setText(String.valueOf(bar.getRating())+" estrellas");
                totalResults.setText("de " + String.valueOf(BarHelper.getTotalResults(true))  +" encontrados");
                float ratingAux = (float)bar.getRating();
                rb_result_rating.setRating(ratingAux);
                rb_result_rating.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                tv_result_state.setVisibility(View.INVISIBLE);
                tv_shakeIt.setVisibility(View.VISIBLE);
                ivBotonFlecha.setVisibility(View.VISIBLE);
                ivBotonFlecha.startAnimation(anim_rotate);
                //Guardamos el estado en randomBar, si giramos el celular setea los siguientes valores nuevamente.
                randomBar.setName(bar.getName());
                randomBar.setVicinity(bar.getVicinity());
                randomBar.setLatitude(bar.getLatitude());
                randomBar.setLongitude(bar.getLongitude());
            }
        });
    }

    private void setBotonQuieroIrYa() {
        ImageView iv_quiero_ir_ya = findViewById(R.id.iv_quiero_ir_ya);
        iv_quiero_ir_ya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!randomBar.getName().equalsIgnoreCase(""))
                {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q="+randomBar.getVicinity()+"&mode="+extras.getString("navigationMode"));
                    //Uri gmmIntentUri = Uri.parse("geo:"+randomBar.getLatitude()+","+randomBar.getLongitude()+"?z=12&q=" + Uri.encode(randomBar.getVicinity()));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            }
        });
    }


    private void setShakeSuggest(){
        TextView tv_shakeIt = (TextView) findViewById(R.id.tv_shakeIt);

        tv_shakeIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast();
            }
        });
    }

    public void showToast() {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_root));

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

}
