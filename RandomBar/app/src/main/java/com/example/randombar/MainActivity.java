package com.example.randombar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.LocationListener;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;

import java.util.List;

public class MainActivity extends AppCompatActivity implements LocationListener {
    //Como mostrar un mensajito :)
    //Toast.makeText(MainActivity.this, "Tu mensaje aca", Toast.LENGTH_LONG).show();
    private LocationManager locationManager;
    private static final int MY_PERMISSIONS_REQUEST_ACCESSLOCATION = 999;
    private Location location = null;

    //Se setean todos los componentes al crear la activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setBotonOpciones();
        setSearchOptions();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Siempre que la app deja de estar activa debo avisarle al GPS que deje de escuchar
        /*
         * El gps consume mucha bateria, prueben comentar la linea de abajo y veran que el simbolo
         * de gps queda encendido, esto sucedera hasta que la app sea descartada por android luego de un
         * periodo largo de inactividad
         * */
        if (locationManager != null)
            locationManager.removeUpdates(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setBotonPricipal();
    }

    @Override
    public void onLocationChanged(Location location) {
        //escribirCalleyNumero(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        //Toast.makeText(MainActivity.this, "Status Changed:" + s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderEnabled(String s) {
        //Toast.makeText(MainActivity.this, "Proveedor Enabled:" + s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        //Toast.makeText(MainActivity.this, "Proveedor Disabled:" + s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESSLOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    iniciarGeolocalizacion();
                } else {
                    //SI NO DA PERSMISOS A LA NOTIFICACION DEL SISTEMA ENTRA ACA, NO COLOCAR NADA.
                }
                return;//WTF bro
            }
        }
    }

    private void setSearchOptions() {
        final SeekBar sb_maxDistance;
        final RatingBar minStars;
        final TextView tv_maxDistance;
        final TextView tv_minStars;
        final ImageView iv_distance_icon;
        final ImageView iv_go;
        final Animation anim_rotate;
        final Animation fadein;

        sb_maxDistance = (SeekBar) findViewById(R.id.sb_maxDistance);
        minStars = (RatingBar) findViewById(R.id.rb_minStars);
        tv_maxDistance = (TextView) findViewById(R.id.tv_maxDistance);
        tv_minStars = (TextView) findViewById(R.id.tv_minStars);
        iv_distance_icon = (ImageView) findViewById(R.id.iv_distance_icon);
        iv_go = (ImageView) findViewById(R.id.iv_go);
        anim_rotate = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate_custom);
        fadein = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fadein);

        tv_maxDistance.setText(getString(R.string.distance_opcion_1));

        minStars.setOnRatingBarChangeListener(
                new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean b) {
                        iv_go.startAnimation(anim_rotate);
                        if (rating == 1) {
                            tv_minStars.setText("Quiero " + (int) rating + " estrella minimo");
                        } else {
                            tv_minStars.setText("Quiero " + (int) rating + " estrellas minimo");
                        }
                    }
                }
        );

        sb_maxDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int chosenValue = sb_maxDistance.getProgress();
                iv_go.startAnimation(anim_rotate);

                switch (chosenValue) {
                    case 0:
                        tv_maxDistance.setText(getString(R.string.distance_opcion_1));
                        iv_distance_icon.setImageResource(R.drawable.caminar);
                        iv_distance_icon.startAnimation(fadein);
                        break;
                    case 1:
                        tv_maxDistance.setText(getString(R.string.distance_opcion_2));
                        iv_distance_icon.setImageResource(R.drawable.ruedas);
                        iv_distance_icon.startAnimation(fadein);
                        break;
                    case 2:
                        tv_maxDistance.setText(getString(R.string.distance_opcion_3));
                        iv_distance_icon.setImageResource(R.drawable.auto);
                        iv_distance_icon.startAnimation(fadein);
                        break;
                    case 3:
                        tv_maxDistance.setText(getString(R.string.distance_opcion_4));
                        iv_distance_icon.setImageResource(R.drawable.rueda);
                        iv_distance_icon.startAnimation(fadein);
                        break;
                    default:
                        tv_maxDistance.setText("WTF, rompiste el codigo ¡Hacker!");
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private String getNavigationMode(){
        SeekBar sb_maxDistance;
        int indice;
        String resul;

        sb_maxDistance = (SeekBar) findViewById(R.id.sb_maxDistance);
        indice = sb_maxDistance.getProgress();

        switch (indice) {
            case 0:
                resul = "w";
                break;
            case 1:
                resul = "b";
                break;
            case 2:
                resul = "d";
                break;
            case 3:
                resul = "d";
                break;
            default:
                resul = "d";
        }
        return resul;
    }
    private int getMaxDistanceFromSeekBar() {
        SeekBar sb_maxDistance;
        int chosenDistance;
        int maxDistance;

        sb_maxDistance = (SeekBar) findViewById(R.id.sb_maxDistance);
        chosenDistance = sb_maxDistance.getProgress();

        switch (chosenDistance) {
            case 0:
                maxDistance = 400;
                break;
            case 1:
                maxDistance = 1500;
                break;
            case 2:
                maxDistance = 3000;
                break;
            case 3:
                maxDistance = 5000;
                break;
            default:
                maxDistance = 400;
        }
        return maxDistance;
    }

    private void setBotonPricipal() {
        ImageView iv_go;
        iv_go = findViewById(R.id.iv_go);

        if (OpcionesActivity.getModoBar()) {
            iv_go.setImageResource(R.drawable.go_bar_2);
        } else {
            if (OpcionesActivity.getModoPizzeria()) {
                iv_go.setImageResource(R.drawable.go_pizzeria);
            }
        }

        iv_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preguntarporPermisos();
            }
        });
    }

    private void setBotonOpciones() {
        ImageView iv_opciones;
        iv_opciones = (ImageView) findViewById(R.id.iv_opciones);
        iv_opciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, OpcionesActivity.class);
                startActivity(i);
            }
        });
    }

    private void preguntarporPermisos() {
        /* solo necesitamos aprobacion manual
        /* cuando el sdk del celular es a partir de Android 6.0 (nivel de API 23)
        /* en este caso pasa directo si el SDK es antiguo
        */
        final String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
        if (ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, permissions[1]) != PackageManager.PERMISSION_GRANTED) {
            //el permiso no fue dado
            // necesita una explicacion del porque usara ese permiso?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION) &&
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                //Crea un 'popup' con opciones
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.gps_request_title))
                        .setMessage(getString(R.string.gps_permission_request_msg))

                        //ACA SE TIENE QUE CREAR LA OPCION DE INGRESAR DIRECCION MANUALMENTE.

                        //Crea una opcion y setea una accion consecuente a su pulsacion.
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(MainActivity.this, permissions, MY_PERMISSIONS_REQUEST_ACCESSLOCATION);
                            }
                        })
                        .create()
                        .show();

            } else {
                //no necesita explicacion, se piden los permisos directamente
                ActivityCompat.requestPermissions(MainActivity.this, permissions, MY_PERMISSIONS_REQUEST_ACCESSLOCATION);
            }
        } else {
            // Los permisos fueron aceptados
            iniciarGeolocalizacion();
        }
    }

    //QUE HACE LA CUANDO NOS PIDE PERSMISOS DE GPS SEGUN LA RESPUESTA

    //Obtiene la ubicacion
    private void iniciarGeolocalizacion() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        final boolean networkProvider = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        String bestProvider = null;

        try {
            if (gpsEnabled) {
                List<String> providers = locationManager.getProviders(false);
                for (String provider : providers) {
                    if (provider.equals("passive")) {
                        bestProvider = provider;
                    }
                }
                location = locationManager.getLastKnownLocation(bestProvider);
                locationManager.requestLocationUpdates(bestProvider, 1000, 1, this);
                if (location != null) {
                    goToResultadoActivity();
                } else {
                    if (networkProvider) {
                        for (String provider : providers) {
                            if (provider.equals("network")) {
                                bestProvider = provider;
                            }
                        }
                        location = locationManager.getLastKnownLocation(bestProvider);
                        locationManager.requestLocationUpdates(bestProvider, 1000, 1, this);
                        if (location != null) {
                            goToResultadoActivity();
                        } else {
                            for (String provider : providers) {
                                if (provider.equals("gps")) {
                                    bestProvider = provider;
                                }
                            }
                            location = locationManager.getLastKnownLocation(bestProvider);
                            locationManager.requestLocationUpdates(bestProvider, 1000, 1, this);
                            if (location != null) {
                                goToResultadoActivity();
                            }
                            else {
                                Toast.makeText(MainActivity.this, getString(R.string.main_activity_location_null), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            } else {
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.gps_request_title))
                        .setMessage(getString(R.string.gps_request_msg))
                        //Crea una opcion y setea una accion consecuente a su pulsacion.
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                        .setNegativeButton("No gracias", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .create()
                        .show();
            }
        } catch (SecurityException e) {
        } catch (Exception ex) {
        }
    }


    private void goToResultadoActivity() {
        Intent i = new Intent(MainActivity.this, ResultadoActivity.class);
        int maxDistance = getMaxDistanceFromSeekBar();
        double minRating = getMinStarsFromRatingBar();
        String navigationMode = getNavigationMode();

        double myLat = location.getLatitude();
        double myLng = location.getLongitude();


        i.putExtra("myLat", myLat);
        i.putExtra("myLng", myLng);
        i.putExtra("maxDistance", maxDistance);
        i.putExtra("minRating", minRating);
        i.putExtra("navigationMode", navigationMode);
        startActivity(i);
    }

    private int getMinStarsFromRatingBar() {
        RatingBar minStars;
        minStars = (RatingBar) findViewById(R.id.rb_minStars);
        return (int) minStars.getRating(); //Me refiero a esta linea
    }






}






