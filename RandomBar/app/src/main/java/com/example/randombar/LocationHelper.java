package com.example.randombar;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.widget.Toast;

import java.util.List;

public class LocationHelper {
    private LocationManager locationManager;
    private Location location = null;

    public LocationHelper(){

    }

    private void iniciarGeolocalizacion() {
        //locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        final boolean networkProvider = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (gpsEnabled) {
            String bestProvider = null;
            List<String> providers = locationManager.getProviders(false);
            for (String provider : providers) {
                if (provider.equals("passive")) {
                    bestProvider = provider;
                }
            }
            if (bestProvider == null) {
                //No devolvio ningun proveedor, entonces...
            }
            //Solo si obtenemos una localizacion avanzamos a la pantalla de resultado.
            try {
                location = locationManager.getLastKnownLocation(bestProvider);
                //locationManager.requestLocationUpdates(bestProvider, 500, 1, this);
                if (location != null) {
                    double myLat = location.getLatitude();
                    double myLng = location.getLongitude();

                } else {
                    //Location vale null, entonces...
                }
            } catch (SecurityException e) {
            } catch (Exception ex) {
            }
        } else {
            //El gps esta inactivo entonces...
        }
    }
}
