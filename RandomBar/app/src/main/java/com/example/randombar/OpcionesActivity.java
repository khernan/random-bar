package com.example.randombar;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

public class OpcionesActivity extends AppCompatActivity {

    private static boolean modoPizzeria = false;
    private static boolean modoPredeterminado = true;
    Animation anim_rotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opciones);
        anim_rotate = AnimationUtils.loadAnimation(OpcionesActivity.this, R.anim.rotate_custom);
        setModos();
    }

    private void setModos() {
        final ImageView modoBarAux = findViewById(R.id.iv_modo_bar);
        final ImageView modoPizzaAux = findViewById(R.id.iv_modo_pizza);
        final LinearLayout ll_modo_bar = findViewById(R.id.ll_modo_bar);
        final LinearLayout ll_modo_pizza = findViewById(R.id.ll_modo_pizza);
        final TextView modeTitle = findViewById(R.id.tv_mode_title);

        if(modoPredeterminado){
            ll_modo_bar.setBackgroundColor(getResources().getColor(R.color.activated));
            ll_modo_pizza.setBackgroundColor(getResources().getColor(R.color.no_color));
            modeTitle.setText(getString(R.string.modo_bar_title));
        }else {
            if(modoPizzeria){
                ll_modo_pizza.setBackgroundColor(getResources().getColor(R.color.activated));
                ll_modo_bar.setBackgroundColor(getResources().getColor(R.color.no_color));
                modeTitle.setText(getString(R.string.modo_pizzeria_title));
            }
        }

        modoBarAux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modoPredeterminado = true;
                modoPizzeria = false;

                modoBarAux.startAnimation(anim_rotate);
                modeTitle.setText(getString(R.string.modo_bar_title));

                ll_modo_bar.setBackgroundColor(getResources().getColor(R.color.activated));
                ll_modo_pizza.setBackgroundColor(getResources().getColor(R.color.no_color));
            }
        });

        modoPizzaAux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modoPizzeria = true;
                modoPredeterminado = false;

                modoPizzaAux.startAnimation(anim_rotate);

                modeTitle.setText(getString(R.string.modo_pizzeria_title));

                ll_modo_pizza.setBackgroundColor(getResources().getColor(R.color.activated));
                ll_modo_bar.setBackgroundColor(getResources().getColor(R.color.no_color));
            }
        });

    }

    public static String getKeyword() {
        String resul = "bar";
        if(modoPizzeria){
            resul = "pizzeria";
        }
        return resul;
    }

    static public boolean getModoPizzeria(){
        return modoPizzeria;
    }

    static public boolean getModoBar(){
        return modoPredeterminado;
    }

}
