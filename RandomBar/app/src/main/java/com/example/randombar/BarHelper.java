package com.example.randombar;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Iterator;

public class BarHelper {
    private static ArrayList<JSONObject> resultadosJson = null;
    private static ArrayList<JSONObject> resultadosJsonFiltrados = null;
    private static Bar lastResult;

    public BarHelper(){
    }

    static public void vaciarResultados(){
        resultadosJson = new ArrayList<JSONObject>();
        resultadosJsonFiltrados = new ArrayList<JSONObject>();
    }

    //Guarda todos los resutados en la lista resultadosJson
    static public void actualizarResultadosJson(JSONArray jsonArray){
        for (int i = 0 ; i <jsonArray.length() ; i++) {
            try {
                resultadosJson.add((JSONObject) jsonArray.get(i));
            } catch (JSONException e) {
            }
        }
    }

    //Revisa la lista resultadosJson y quita los json que no cumplan con el minimo de rating
    static public void filtrarResultados(double minRating){
        double barRating = 0;

        for(JSONObject bar : resultadosJson){
            try {
                barRating = bar.getDouble("rating");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(barRating >= minRating){
                resultadosJsonFiltrados.add(bar);
            }
        }
    }

    //Devuelve un bar random de la lista resultadosJson convertida a objeto Bar o null si no encuentra nada
    static public Bar getRandomBar(boolean filtered){
        Bar resul = null;
        JSONObject chosenBar = (JSONObject) getRandomBarJson(filtered);
        //No reemplazar este if por un try catch de NullPointException, sino devuelve un bar vacio.
        if(chosenBar != null){
            try {
                resul = new Bar();
                //Guardamos los valores del bar elegido en nuestra variable Bar bar.
                resul.setName(chosenBar.getString("name"));
                resul.setLatitude(chosenBar.getJSONObject("geometry").getJSONObject("location").getDouble("lat"));
                resul.setLongitude(chosenBar.getJSONObject("geometry").getJSONObject("location").getDouble("lng"));
                resul.setId(chosenBar.getString("id"));
                resul.setPlace_id(chosenBar.getString("place_id"));
                resul.setRating(chosenBar.getDouble("rating"));
                resul.setVicinity(chosenBar.getString("vicinity"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        lastResult = resul;
        return resul;
    }

    //Devuelve un bar random de la lista resultadosJson
    static private JSONObject getRandomBarJson(boolean filtered){
        JSONObject resul = null;
        int randomIndex;

        if(!resultadosJsonFiltrados.isEmpty() && filtered){
            randomIndex = (int)( Math.random() * resultadosJsonFiltrados.size());
            resul = (JSONObject) resultadosJsonFiltrados.get(randomIndex);
        }
        if(!resultadosJson.isEmpty()&& !filtered){
            randomIndex = (int)( Math.random() * resultadosJson.size());
            resul = (JSONObject) resultadosJson.get(randomIndex);
        }
        return resul;
    }

    static public int getTotalResults(boolean filtered){
        int resul = 0;
        if(filtered) {
            resul = resultadosJsonFiltrados.size();
        } else{
            resul = resultadosJson.size();
        }
        return resul;
    }

    static public Bar getLastResult(){
        return lastResult;
    }
}


